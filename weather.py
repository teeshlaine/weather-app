
def add_style(display_function):
    def wrapper(*args, **kwargs):
        output = '<div style="background-color: #CCC; padding: 10px;">'
        output += display_function(*args, **kwargs)
        output += '</div>'
        return output
    return wrapper

@add_style
def display_weather(data):
    output = f'<p>Temperature: {data["temperature"]}</p>'
    output += f'<p>Humidity: {data["humidity"]}</p>'
    output += f'<p>Wind Speed: {data["wind_speed"]}</p>'
    return output



# import required modules
from pkg_resources import run_script
import requests

from flask import Flask, render_template

# Enter your API key here
api_key = "8a2fb5edf233f9661add54e4151cdfa8"

# base_url variable to store url
base_url = "http://api.openweathermap.org/data/2.5/weather?"

# Give city name
city_name = input("Enter city name : ")

# complete_url variable to store
# complete url address
complete_url = base_url + "appid=" + api_key + "&q=" + city_name

# get method of requests module
# return response object
response = requests.get(complete_url)

# json method of response object
# convert json format data into
# python format data
x = response.json()

# Now x contains list of nested dictionaries
# Check the value of "cod" key is equal to
# "404", means city is found otherwise,
# city is not found
if x["cod"] != "404":

	# store the value of "main"
	# key in variable y
	y = x["main"]

	# store the value corresponding
	# to the "temp" key of y
	current_temperature = y["temp"]

	# store the value corresponding
	# to the "pressure" key of y
	current_pressure = y["pressure"]

	# store the value corresponding
	# to the "humidity" key of y
	current_humidity = y["humidity"]

	# store the value of "weather"
	# key in variable z
	z = x["weather"]

	# store the value corresponding
	# to the "description" key at
	# the 0th index of z
	weather_description = z[0]["description"]

	# print following values
	print(" Temperature (in kelvin unit) = " +
					str(current_temperature) +
		"\n atmospheric pressure (in hPa unit) = " +
					str(current_pressure) +
		"\n humidity (in percentage) = " +
					str(current_humidity) +
		"\n description = " +
					str(weather_description))

else:
	print(" City Not Found ")


    
app = Flask(__name__)

@app.route('/')
def index():
    # Run your Python script here
    # and store the results in a variable
    data = run_script()
    return render_template('index.html', data=data)

if __name__ == '__main__':
    app.run()
